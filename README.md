# Requisitos

Implemente uma funcionalidade que recebe um id de usuário como argumento, obtém dados deste usuário de uma API e, dependendo do papel do usuário (role), retorna um de dois dados:
* Se o papel for admin, retorna o nome, papel, a senha e suas permissões.
As permissões não são obtidas da API, mas sim de um objeto de permissões (right). Embora este objeto tem as permissões em formato camelCase, elas devem ser informadas em pascal-case no retorno.
* Se o papel for regular, retorna apenas o nome e o papel.

Tanto as permissões quanto os papéis podem mudar no futuro.